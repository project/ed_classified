
Simple Text-based classified ads module for Drupal.

The "master" branch is not used. Please checkout one of the actual per-Drupal-version branches:

- Drupal 7
  - 7.x-3.x is the current branch, where new code goes
  - 7.x-2.x is an obsolete experimental branch, based on 5.x-2.x
- Drupal 6
  - 6.x-3.x is the maintained branch for Drupal 6.
  - 6.x-2.x was the previous active branch, based on 5.x-2.x
  - 6.x-2.0-alpha6 is the most used release
- Drupal 5 (obsolete since 2011-01-05)
  - 5.x-2.x was an experimental branch on multi-version compatibility
  - 5.x-1.x was the production branch for Drupal 5
  - 5.x-1.6 is the final release for Drupal 5 and should be used until upgrading to Drupal 6/7
- Drupal 4.7 (obsolete since 2007-01-15)
  - 4.7.x-1.x was the only branch for Drupal 4.7
  - 4.7-1.5.3 was the final production release for Drupal 4.7
